/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.HashMap;
import java.util.Map;


public class MainBean {
    private Map<String, String> waluty2 ; 
    private EnumWaluty walutaZrodlowa;
    
    
    public Map<String, String> getWaluty2() {
        
     if(waluty2!=null && waluty2.size()>0)
         return waluty2;
     
     
        waluty2=new HashMap<String,String>();
        
        Object[] walutyValues=EnumWaluty.class.getEnumConstants();
        
        for(Object walutaLabel : walutyValues){
        
            waluty2.put(String.valueOf(walutaLabel), "");
        }
        
        
        return waluty2;
    }

    /**
     * @param waluty the waluty to set
     */
    public void setWaluty2(Map<String, String> waluty) {
        this.waluty2 = waluty;
    }
    
        /**
     * @return the walutaZrodlowa
     */
    public EnumWaluty getWalutaZrodlowa() {
        return walutaZrodlowa;
    }

    /**
     * @param walutaZrodlowa the walutaZrodlowa to set
     */
    public void setWalutaZrodlowa(EnumWaluty walutaZrodlowa) {
        this.walutaZrodlowa = walutaZrodlowa;
    }
    
}
