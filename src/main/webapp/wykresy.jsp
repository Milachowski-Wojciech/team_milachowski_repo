

<%@page import="beans.MainBean"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Wykresy</title>
    </head>
    <body>
        <h1>Wykresy walutowe</h1>
        <br/>
    <a href="index.jsp">Powrót do menu</a>
    <br/><br/>
    <form method="post" action="/ChartServlet">
        
        <select name="walutaIn">
<%
Map<String, String> waluty2 = ((MainBean)request.getAttribute("pb")).getWaluty2();

for(Entry<String, String> waluta2 : waluty2.entrySet()) {
%>

    <option value="<%=waluta2.getKey()%>" <%=waluta2.getValue()%>>
        <%=waluta2.getKey()%>
    </option>

<%
}
%>

 </select>
        
        <button type="submit">Generuj</button>
        

        
        
    </form>
    </body>
</html>
