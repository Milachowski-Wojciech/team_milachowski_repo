
<%@page import="beans.PrzelicznikBean"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<html>
    
    <head>
        <title>Kalkulator walut</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
             
        <h1>Kalkulator walut</h1>
        
        <br/>
        <a href="index.jsp">Powrót do menu</a>
        <br/>
        <form method="post" action="/MainServlet">
          
            <table>
                <tr><td>kwota do przewalutowania</td><td><input type="text" value="${pb.kwota}" name="kwota"/></td></tr>
                
                <tr><td>waluta źródłowa</td><td>
                    
    <select name="walutaIn">
<%
Map<String, String> waluty1 = ((PrzelicznikBean)request.getAttribute("pb")).getWaluty1();

for(Entry<String, String> waluta1 : waluty1.entrySet()) {
%>

    <option value="<%=waluta1.getKey()%>" <%=waluta1.getValue()%>>
        <%=waluta1.getKey()%>
    </option>

<%
}
%>

 </select>
                    </td></tr>
                
                
                <tr><td>waluta docelowa</td><td>
<select name="walutaOut">
<%
Map<String, String> waluty2 = ((PrzelicznikBean)request.getAttribute("pb")).getWaluty2();

for(Entry<String, String> waluta2 : waluty2.entrySet()) {
%>

    <option value="<%=waluta2.getKey()%>" <%=waluta2.getValue()%>>
        <%=waluta2.getKey()%>
    </option>

<%
}
%>

 </select></td></tr>
                   
                  <tr><td>kwota po przewalutowaniu</td><td><input type="text" value="${pb.wynik}" name="wynik"/></td></tr>
            </table>
            
            
            
            <button type="submit">Przelicz</button>
        </form>
        
        
    </body>
</html>
